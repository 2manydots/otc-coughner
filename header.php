<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7]>   <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]>   <html class="no-js lt-ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title><?php if ( !is_front_page() ) { wp_title(''); } else { bloginfo('name'); } ?></title>
    <?php wp_head(); ?>
    <link href='https://fonts.googleapis.com/css?family=Dosis:600' rel='stylesheet' type='text/css'>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-73963748-3', 'auto');
      ga('send', 'pageview');

    </script>
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
</head>
<body <?php body_class(); ?>>
    
            
        
    <header class="header">
        <div class="outer-wrapper">
            <div class="row">
                <div class="large-3 medium-12 columns logo-section">
                    
                        <a class="logo" href="<?= home_url(); ?>" rel="nofollow"><?php new Sprite('cougner-logo'); ?></a>

                </div>
                <div class="large-6 medium-12 columns top-nav-section">                    
                    <?php wp_nav_menu(array('menu_class' => 'main-nav', 'theme_location' => 'main-nav')) ?>                    
                </div>
                <div class="large-3 medium-12 columns shopping-cart-wrapper">
                    <?php if(is_page_template( 'templates/cart-checkout.php' )){?>
                        <span class="shopping-cart-button active">Your Shoppingbag</span>
                    <?php }else {?>
                        <span class="shopping-cart-button">Your Shoppingbag</span>
                        <?php if ( is_active_sidebar( 'shopping-cart' ) ) :?>
                            <div class="shopping-cart-popup">
                                <div class="cart">
                                    <?php dynamic_sidebar( 'shopping-cart' );?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php }?>

                </div>
            </div>
        </div>
    </header>