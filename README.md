# WordPress  Grunt Boilerplate

An example of the boilerplate can be found [here](http://boilerplate.flywheelsites.com).

## Changelog

| Version | Comment                       | Author          |
| ------- | ----------------------------- | --------------- |
| 0.3     | Improvements for the plugin   | Patrick Leijser |
| 0.2     | Suggestions Victor merged     | Patrick Leijser |
| 0.1     | Initial GRUNT Boilerplate     | Patrick Leijser |