(function ($) {
  
    $(function () {





    // //Slide to anchor section
    //     $('.main-nav li a').on('click', function(e) {
    //         var clearURL = $(this).attr('href').replace('/', '');
    //         console.log(clearURL);
    //          $('html, body').animate({
    //              scrollTop: $(clearURL).offset().top + 'px'
    //          }, 500);
    //          return false;
    //      });
     // *only* if we have anchor on the url

    //Iframe 16:9 resize
        var iframe_width = $('.iframe_for_video iframe').width();
        $('.iframe_for_video iframe').css('height', iframe_width * 0.5625 + 'px');
    //UL OL fix for previous element margin
        $( "ul, ol" ).prev("p").css( "margin-bottom", "0" );

        //Accordion
        function accordionToggle() {
            $('.accordion .accordion-toggle').off('click');
            $('.accordion').each(function () {
                $(".accordion-content").hide();
                $('.accordion-toggle:first-of-type').addClass('active');
                $('.accordion-toggle:first-of-type').next().show().addClass('active');
                $(this).find('.accordion-toggle').click(function () {
                    if($(this).hasClass('active')){
                        $(this).removeClass('active');
                        $(this).parent('.accordion').find('.accordion-content').slideUp('fast');
                    } else {
                        $(this).parent('.accordion').find('.accordion-content').slideUp('fast');
                        $(this).parent('.accordion').find('.accordion-toggle').removeClass('active');
                        $(this).addClass('active');
                        $(this).next().slideDown('fast').addClass('active');
                        $(".accordion-content").removeClass('active');
                    }
                });
            });
        }
        accordionToggle();

        //Slider
        $('.top_slider_wrapper').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 500,
            dots:false,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows:false
        });
        //Shopping cart popup
        $('.shopping-cart-button').on('click', function() {
            $('.shopping-cart-popup').toggleClass('active');
        });

        // window.localStorage.removeItem("priceStorage");
        // Add to Cart
        $('body').on('click', 'span.btn_add', function() {


            var quantity_value = parseInt('1');
            if($('.quantity').val()) {
                quantity_value = parseInt($('.quantity').val());
            }
            var quantity = parseInt('1');
            if(quantity_value) {
                quantity = quantity_value;
            }

            // var delivery = 5;
            var featured_width = $('.featured_image').data('width');
            var featured_height = $('.featured_image').data('height');
            var featured_small = $('.featured_image').data('small');
            var featured_big = $('.featured_image').data('big');

            var price = +$(this).attr('data-price') ;
            var title = $(this).attr('data-title');
            var total_pro_price =+ $('.total-pro-price').text();
            total_pro_price  = (total_pro_price + price * quantity).toFixed(2);

            $('.total-pro-price').text(total_pro_price);

            var goNext = true;

            $('.merchant-list li').each(function() {
                if(title == $(this).find('.pro-name').text()) {
                    var thisCount = +$(this).find('.pro-count').text();
                    if(quantity) {
                        thisCount = thisCount + quantity;
                    } else {
                        thisCount++;
                    }
                    $(this).find('.pro-count').text(thisCount);
                    goNext = false;
                    localStorage.setItem('priceStorage', JSON.stringify(jQuery('.cart-memory').html()));
                }

            });

            if (goNext) {
                var stringToBasket =
                    '<li>' +
                        '<div class="featured-img"><img width="' + featured_width/2 + '" height="' + featured_height/2 + '" class="image_element" src="' + featured_small  + '" srcset="' + featured_big + ' 2x,' + featured_small + '  1x" alt="' + title + '"></div>' +
                        '<div class="cart-pro-title">' +
                            '<span class="pro-name">' + title + '</span><span class="pro-count">' + quantity + '</span>' +
                        '</div>' +
                        '<div class="cart-pro-price">' +
                            '€<span class="pro-price">' + price.toFixed(2) + '</span>' +
                            '<a href="javascript:void(0)" class="pro-close"></a>' +
                        '</div>' +
                    '</li>';
                $('.merchant-list').append(stringToBasket);
                localStorage.setItem('priceStorage', JSON.stringify(jQuery('.cart-memory').html()));
            }

            if ($('.merchant-list li').length > 0) {
                //$('.empty-cart').hide();
                $('.total-pro').show();
                //$(".smbt-shop input[type=submit]").prop('disabled', false);
            }
            else {
                //$('.empty-cart').show();
                //$(".smbt-shop input[type=submit]").prop('disabled', true);
            }

            $('#cartinput').html(renderCartFormInput());

        });

        // Delete from cart
        $('body').on('click', '.shopping-cart-big a.pro-close', function() {
            var mult = +$(this).parent().prev().find('.pro-count').text();
            var price = +($(this).prev().text())*mult;
            var minus_price = +$('.total-pro-price').text();
            if(minus_price) { minus_price  = (minus_price - price).toFixed(2); }
            else { minus_price = price.toFixed(2); }
            $('.total-pro-price').text(minus_price);
            $(this).closest('li').remove();
            localStorage.setItem('priceStorage', JSON.stringify(jQuery('.cart-memory').html()));

            if ($('.merchant-list li').length > 0) {
                //$('.empty-cart').hide();
                $('.total-pro').show();
                //$(".smbt-shop input[type=submit]").prop('disabled', false);
            }
            else {
                //$('.empty-cart').show();
                $('.total-pro').hide();
                //$(".smbt-shop input[type=submit]").prop('disabled', true);
            }

            $('#cartinput').html(renderCartFormInput());

        });

        if ($('.cart-memory') && typeof localStorage.getItem('priceStorage') !== 'undefined' && localStorage.getItem('priceStorage') !== null) {
            var retrievedObject = localStorage.getItem('priceStorage');
            var neWretrievedObject = retrievedObject.replace(/\\"/g, '"');
            var neWretrievedObject2 = neWretrievedObject.replace(/"\\n/g, '');
            var neWretrievedObject3 = neWretrievedObject2.replace(/\\n/g, '');
            var neWretrievedObject4 = neWretrievedObject3.replace(/\>\"/g, '>');
            var neWretrievedObject5 = neWretrievedObject4.replace(/  "/g, '');
            var neWretrievedObject6 = neWretrievedObject5.replace(/"   /g, '');
            //console.log(neWretrievedObject6);
            //console.log('not empty');
            $('.cart-memory').html(neWretrievedObject6);
            $('.total-pro').show();

            $('#cartinput').html(renderCartFormInput());

            //localStorage.clear();
        } else {
            //console.log('Storage is Empty!');
        }

    //Render Cart Form Input
        function renderCartFormInput() {
            var result = '';
            var total = $('.cart-memory .total-pro-price').text();

            $('.cart-memory .merchant-list').find('li').each(function() {
                var _self = $(this);
                var count = $(this).find('.pro-count').text();
                var title = $(this).find('.pro-name').text();
                var price = $(this).find('.pro-price').text();

                result = result + count + 'x ' + title + ' - &euro;' + price + '\n';
            });

            result = result + '\n' + 'Totaal: &euro;' + total;

            // showQuotationBtn();

            return result;
        }


        // $('.shop-form-box form').find('input.wpcf7-validates-as-required').each(function() {
        //     var _self = $(this);
        //
        //     _self.on('change', function() {
        //         showQuotationBtn();
        //     });
        // });
        //
        // $('#confirm-box input[type=checkbox]').on('change', function() {
        //     showQuotationBtn();
        // });
        //
        // function showQuotationBtn() {
        //     // Init elements
        //     var checkBox = $('#confirm-box input[type=checkbox]');
        //     var cartSubmit = $('.smbt-shop input[type=submit]');
        //
        //     // Set disabled on start
        //     cartSubmit.prop('disabled', true);
        //
        //     // Check if terms are agreed and cart is not empty
        //     if ( checkBox.is(':checked') && $('.merchant-list li').length > 0 ) {
        //         var empty = false;
        //
        //         // Foreach trough all required fields and check if empty
        //         $('.shop-form-box input.wpcf7-validates-as-required').each(function() {
        //             if($(this).val() === '') {
        //                 empty = true;
        //                 console.log($(this));
        //             }
        //         });
        //         // If all required fields are filled in
        //         if(!empty) {
        //             cartSubmit.prop('disabled', false);
        //             $('.form-full-message').hide();
        //         } else {
        //             cartSubmit.prop('disabled', true);
        //             $('.form-full-message').show();
        //         }
        //     }
        //     else {
        //         cartSubmit.prop('disabled', true);
        //         $('.form-full-message').show();
        //     }
        // }


        //Match height
        $('.text-wrapper .top-equal').matchHeight();
        $('.list-wrapper .list-item').matchHeight();
        $('.section-cell').matchHeight();
        $('.products-post-type .featured_image').matchHeight();
        $('.products-post-type .product-wrapper').matchHeight();
        $('.woocommerce .before_shop_loop_wrapper').matchHeight();
        $('.woocommerce ul.products li.product a').matchHeight();
        $('.shop_loop_item_description').matchHeight();

        $('.ajax_add_to_cart').click(function () {

        });
        $('body').on('added_to_cart',function(){
            $('.shopping-cart-popup').addClass('active');
            $('html,body').animate({scrollTop: 0});
        });

        $(window).load(function () {
            //Woocommerce cart popup after adding product from single page
            if( $('.woocomerce-message').length){
                alert('fdf');
                $('.shopping-cart-popup').addClass('active');
            }

            //Product Image Margins
            $(".product-preview").each(function () {
                var product_height = $(this).height();
                    margin_bottom = product_height/2.8;
                $(this).css({'bottom' : -margin_bottom});
                $('.top-image').css({'margin-bottom' : margin_bottom});
            });
            $(".image-section").each(function () {
                var image_section_width = $(this).width();
                    image_section_height = image_section_width/4.47;
                $(this).css({'min-height' : image_section_height});
            });
            if(window.location.hash) {
                 $('html, body').animate({
                     scrollTop: $(window.location.hash).offset().top + 'px'
                 }, 500);
             }

        });
        $(window).resize(function () {
            //Iframe 16:9 resize
            var iframe_width = $('.iframe_for_video iframe').width();
            $('.iframe_for_video iframe').css('height', iframe_width * 0.5625 + 'px');
            //Match height
            $('.description-section .equal-height').matchHeight();
            $('.text-wrapper .columns').matchHeight();
            //Product Image Margins
            $(".product-preview").each(function () {
                var product_height = $(this).height();
                    margin_bottom = product_height/2.8;
                $(this).css({'bottom' : -margin_bottom});
                $('.top-image').css({'margin-bottom' : margin_bottom});

            });
            $(".image-section").each(function () {
                var image_section_width = $(this).width();
                    image_section_height = image_section_width/4.47;
                $(this).css({'min-height' : image_section_height});
            });
        });
    });




})(jQuery);