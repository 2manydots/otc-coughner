<?php
/**

 */

get_header(); ?>
    <main class="main">

        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>

            <?php endwhile;?>
        <?php endif; ?>
        <div class="row">
            <div class="large-10 medium-10 medium-offset-1 small-12 columns">
                <?php woocommerce_content(); ?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>