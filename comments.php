<?php
/**
 * The template for displaying the index file
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */
?>

<div class="row">
    <div class="medium-12 columns">
        <span class="h3">
            <?php
            printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'tmd-wp-grunt' ),
                number_format_i18n( get_comments_number() ), get_the_title() );
            ?>
        </span>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
        <ol class="comment-list">
            <?php
            wp_list_comments( array(
                'style'       => 'ol',
                'short_ping'  => true,
                'avatar_size' => 56,
            ) );
            ?>
        </ol>
    </div>
</div>

<?php if ( comments_open() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
    <div class="row">
        <div class="medium-12 columns">
            <?php comment_form(); ?>
        </div>
    </div>
<?php endif; ?>