<?php

// Theme support
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

// Custom image sizes
add_image_size( 'home-top', 4096, 585, array( 'center', 'center' ) );
add_image_size( 'narrow-section-image', 138, 0, false );
add_image_size( 'narrow-section-image-2', 272, 0, false );
add_image_size( 'image-second-section', 920, 880, false );
add_image_size( 'preview-top', 1154, 1120, false);
add_image_size( 'image-section', 1920, 0, false );
add_image_size( 'image-section-2', 3840, 0, false );
add_image_size( 'slider-mobile', 768, 0, false);
add_image_size( 'slider-mobile-2', 1536, 0, false );
add_image_size( 'wide-content-image', 580, 0, false );
add_image_size( 'wide-content-image-2', 1160, 0, false );
add_image_size( 'medium-content-image', 480, 0, false );
add_image_size( 'medium-content-image-2', 960, 0, false );
add_image_size( 'small-content-image', 180, 0, false );
add_image_size( 'small-content-image-2', 360, 0, false );
add_image_size( 'product-thumbnail', 300, 315, false );
add_image_size( 'product-thumbnail-2', 600, 630, false );
add_image_size( 'product-thumbnail-small', 178, 170, false );
add_image_size( 'product-thumbnail-small-2', 356, 340, false );


// add_image_size ( string $name, int $width, int $height, bool|array $crop = false )

// Navigations
function tmd_register_nav_menus() {

    $locations = array(
        'top-nav' => 'Top navigation',
        'main-nav' => 'Main navigation',
        'footer-nav' => 'Footer navigation',
        'mobile-nav' => 'Mobile navigation',
    );
    register_nav_menus( $locations );

}
add_action( 'init', 'tmd_register_nav_menus' );

// Sidebars
function tmd_register_sidebars() {

    $args = array(
        'id'            => 'shopping-cart',
        'name'          => 'Shopping Cart',
        'description'   => 'Description',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<span class="cart-title">',
        'after_title'   => '</span>'
    );
    register_sidebar( $args );

}
add_action( 'widgets_init', 'tmd_register_sidebars' );

// clear dashboard
function remove_menus() {
  remove_menu_page('edit-comments.php');       
  remove_menu_page('edit.php');   
}
add_action('admin_menu', 'remove_menus');

// clear wp admin bar
function remove_wp_nodes() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_node('new-post');
  $wp_admin_bar->remove_node('comments');
}
add_action('admin_bar_menu', 'remove_wp_nodes', 999);

// Theme Settings page
if (function_exists('acf_add_options_page')) {

  acf_add_options_page(array(
      'page_title' => 'Theme Settings',
      'menu_title' => 'Theme Settings',
      'menu_slug' => 'theme-settings',
      'capability' => 'edit_posts',
      'redirect' => false
  ));
}

// button shortcode
// [button link=""]text[/button]
function button_func($atts, $content = null) {


  $attr = shortcode_atts(array(
      'link' => NULL,
          ), $atts);

  $source = $attr['link'];
  $source = '<a href="' . $attr['link'] . '" class="btn shortcodebutton">' . $content . '</a>';
  return $source;
}
add_shortcode('button', 'button_func');

// New tab button shortcode
// [button-n link=""]text[/button-n]
function button_n_func($atts, $content = null) {


  $attr = shortcode_atts(array(
      'link' => NULL,
          ), $atts);

  $source = $attr['link'];
  $source = '<a href="' . $attr['link'] . '" target="_blank" class="btn"><span>' . $content . '</span></a>';
  return $source;
}
add_shortcode('button-n', 'button_n_func');

// custom excerpt
function custom_length_excerpt($word_count_limit) {
    $content = wp_strip_all_tags(get_the_content() , true );
    echo wp_trim_words($content, $word_count_limit);
}

// show admin bar only for admins and editors
if (current_user_can('edit_posts')) {
	add_filter('show_admin_bar', '__return_true');
} 


function rkv_remove_linkmanager() {
	$enabled = get_option( 'link_manager_enabled' );
	if ( 0 !== $enabled  )
		update_option( 'link_manager_enabled', 0 );
}
add_action('admin_init', 'rkv_remove_linkmanager');

update_option( 'link_manager_enabled', 0 );



//Woocommerce
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
//Add short description to shop page
add_action('woocommerce_after_shop_loop_item_title','woocommerce_template_single_excerpt', 5);
//add_action('woocommerce-tabs','woocommerce_template_single_excerpt', 5);


//remove add to cart button
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
//remove short description near image
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
function woocommerce_template_product_description() {
    woocommerce_get_template( 'single-product/tabs/description.php' );
}
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );

//Change single cart button to product page loop with ajax button
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_loop_add_to_cart', 30 );

//Remove title near image
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

//move description near image from tabs
add_filter( 'woocommerce_product_description_heading', 'remove_product_description_heading' );
function remove_product_description_heading() {
    return '';
}

//Delete Tabs
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['description'] ); 			// Remove the reviews tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}


//Change Add to cart text
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +

function woo_custom_cart_button_text() {

    return __( 'Bestellen', 'woocommerce' );

}

