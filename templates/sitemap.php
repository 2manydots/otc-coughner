<?php
/* Template name: Sitemap */

get_header();
?>
<main class="main">

  <div class="row">
    <div class="medium-12 columns">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          <div class="row">
            <div class="large-12 columns">
              <?php the_content(); ?>
            </div>
          </div><!-- row -->


          <div class="row">
            <div class="large-6 columns">
              <h2 class="title_search">Pages</h2>
              <ul class="list_sitemap">
                <?php
                $output = wp_list_pages('echo=0&depth=3&title_li=');
                if (is_page()) {
                  $page = $post->ID;
                  if ($post->post_parent) {
                    $page = $post->post_parent;
                  }
                  $children = wp_list_pages('echo=0&child_of=' . $page . '&title_li=');
                  if ($children) {
                    $output = wp_list_pages('echo=0&child_of=' . $page . '&title_li=');
                  }
                }
                echo $output;
                ?>
              </ul>
            </div>
            <div class="large-6 columns">
              <h2 class="title_search">Blog</h2>
              <ul class="list_sitemap">
                <?php
                $myposts = get_posts('numberposts=-1&post_type=post');
                foreach ($myposts as $post) :
                  ?>
                  <li class="sitemap"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                  <?php endforeach; ?>
              </ul>
            </div>
          </div><!-- row -->



          <?php
        endwhile;
      endif;
      ?>

    </div>
  </div>
</main>
<?php
get_footer();
