<?php
/* Template name: Products */

get_header();
?>
<main class="main">


    <?php
    $image_or_slider = get_field('image_or_slider');
    if($image_or_slider == 'image'){?>
        <?php if($top_image = get_field('top_image')){?>
            <?php
            $image_size = 'image-section';
            $image_size_2 = 'image-section-2';
            $image_alt = $top_image['alt'];
            $image_thumb = $top_image['sizes'][ $image_size ];
            $image_thumb_2 = $top_image['sizes'][ $image_size_2 ];
            $width = ($top_image['sizes'][ $image_size_2 . '-width' ])/2;
            $height = ($top_image['sizes'][ $image_size_2 . '-height' ])/2;
            ?>
            <div class="top_image_wrapper">
                <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                     srcset="<?php echo $image_thumb_2; ?> 2x,
                                <?php echo $image_thumb; ?> 1x"
                     alt="<?php echo $image_left_alt;?>">
                <?php if(get_field('show_title')){?>
                    <div class="row top_image_title_wrapper">
                        <div class="large-12 medium-12 small-12 columns">
                            <h1><?php the_title();?></h1>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } else { ?>
            <?php if(get_field('show_title')){?>
                <div class="row page-title">
                    <div class="large-12 medium-12 small-12 columns">
                        <h1><?php the_title();?></h1>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    <?php } elseif($image_or_slider == 'slider') {?>
        <?php if( have_rows('top_slider') ):?>
            <div class="top_slider_wrapper desktop">
                <?php while ( have_rows('top_slider') ) : the_row();?>
                    <div class="slick-slide">
                        <?php if($image = get_sub_field('desktop_image')){?>
                            <?php
                            $image_size = 'image-section';
                            $image_size_2 = 'image-section-2';
                            $image_alt = $image['alt'];
                            $image_thumb = $image['sizes'][ $image_size ];
                            $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                            $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                            $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                            ?>
                            <div class="desktop_image image_wrapper">
                                <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                     srcset="<?php echo $image_thumb_2; ?> 2x,
                                                    <?php echo $image_thumb; ?> 1x"
                                     alt="<?php echo $image_left_alt;?>">
                            </div>
                        <?php } ?>
                    </div>
                <?php endwhile;?>
            </div>
            <div class="top_slider_wrapper mobile">
                <?php while ( have_rows('top_slider') ) : the_row();?>
                    <div class="slick-slide">
                        <?php if($image = get_sub_field('mobile_image')){?>
                            <?php
                            $image_size = 'slider-mobile';
                            $image_size_2 = 'slider-mobile-2';
                            $image_alt = $image['alt'];
                            $image_thumb = $image['sizes'][ $image_size ];
                            $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                            $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                            $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                            ?>
                            <div class="desktop_image image_wrapper">
                                <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                     srcset="<?php echo $image_thumb_2; ?> 2x,
                                                    <?php echo $image_thumb; ?> 1x"
                                     alt="<?php echo $image_left_alt;?>">
                            </div>
                        <?php } ?>
                    </div>
                <?php endwhile;?>
            </div>
        <?php endif;?>
    <?php } else {?>
        <div class="row page-title">
            <div class="large-12 medium-12 small-12 columns">
                <h1><?php the_title();?></h1>
            </div>
        </div>
    <?php }; ?>



    <div class="row">
        <div class="medium-10 medium-push-1 columns">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php $arg = array(
                    'post_type'	    => 'products', /*<-- Enter name of Custom Post Type here*/
                    'order'		    => 'ASC',
                    'orderby'	    => 'menu_order',
                    'posts_per_page'    => -1
                );
                $the_query = new WP_Query( $arg );
                if ( $the_query->have_posts() ) : ?>
                    <div id="products-post-type" class="products-post-type row">
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="large-6 medium-6 small-12 columns">
                                <div class="product-wrapper">
                                    <?php if($image = get_field('featured_image')){?>
                                        <?php
                                        $image_size = 'product-thumbnail';
                                        $image_size_2 = 'product-thumbnail-2';
                                        $image_alt = $image['alt'];
                                        $image_thumb = $image['sizes'][ $image_size ];
                                        $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                        $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                        $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                        ?>
                                        <a href="<?php the_permalink();?>" class="featured_image image_wrapper">
                                            <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                 srcset="<?php echo $image_thumb_2; ?> 2x,
                                                     <?php echo $image_thumb; ?> 1x"
                                                 alt="<?php echo $image_left_alt;?>">
                                        </a>
                                    <?php } ?>

                                    <div class="product-description">
                                        <h3 class="product-title">
                                            <?php
                                                $title_for_display_blue = get_field('title_for_display_blue');
                                                $title_for_display_black = get_field('title_for_display_black');
                                            ?>
                                            <?php if($title_for_display_blue || $title_for_display_black){?>
                                                <?php if($title_for_display_blue){?>
                                                    <span class="title_for_display_blue"><?php echo $title_for_display_blue;?></span>
                                                <?php }; ?>
                                                <?php if($title_for_display_black){?>
                                                    <span class="title_for_display_black"><?php echo $title_for_display_black;?></span>
                                                <?php }; ?>
                                            <?php } ?>
                                        </h3>
                                        <?php if($short_description = get_field('short_description')){?>
                                            <div class="short_description"><?php echo $short_description;?></div>
                                        <?php } ?>
                                        <a class="btn btn_blue" href="<?php the_permalink();?>">SEE DETAILS</a>
                                    </div>

                                </div>
                            </div>

                        <?php endwhile; ?>
                    </div>
                <?php endif; wp_reset_query(); ?>
            <?php endwhile; endif;?>
        </div>
    </div>
</main>
<?php
get_footer();
