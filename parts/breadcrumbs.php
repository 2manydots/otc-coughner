<?php
/**
 * The partial template for displaying breadcrumbs
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>

<?php if ( function_exists('yoast_breadcrumb') ) : ?>
    <div class="row">
        <div class="medium-12 columns">
            <?php yoast_breadcrumb('<p id="breadcrumbs">','</p>'); ?>
        </div>
    </div>
<?php endif; ?>